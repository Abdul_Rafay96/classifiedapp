//
//  CardView.swift
//  Qarbook Passenger
//
//  Created by Mac on 18/05/2018.
//  Copyright © 2018 Qarbook. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable public class CardView: UIView {
    
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        
    }
    override public func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    
    }
    
}


@IBDesignable public class CardButton: UIButton {
    
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0
  
    @IBInspectable var fullCorner: Bool = false
    
    public override func awakeFromNib() {
           super.awakeFromNib()

        
       }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.buttonConfiguration()
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.buttonConfiguration()
        
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.buttonConfiguration()
    }
//    override public func layoutSubviews() {
//
//    }
    private func buttonConfiguration(){
        layer.cornerRadius = cornerRadius
               let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
               
               layer.masksToBounds = false
               layer.shadowColor = shadowColor?.cgColor
               layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
               layer.shadowOpacity = shadowOpacity
               layer.shadowPath = shadowPath.cgPath
               
               
               if self.fullCorner{
               
                   self.layer.cornerRadius = self.frame.height/2
               }
//        self.layoutIfNeeded()
    }
}


@IBDesignable public class CustomView: UIView {
    
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0
    
    @IBInspectable var fullCorner: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        
    }
    override public func layoutSubviews() {
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        
    }
    
}



extension UIView{
    
    
    
    func borderColor(color:UIColor) {
        self.layer.borderColor = color.cgColor
    }
    
    @IBInspectable var cornerRadius: CGFloat{
        get{
            return layer.cornerRadius
        }
        set{
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
    @IBInspectable var fullCornerRadius: Bool{
        get{
            return false
        }
        set{
            if newValue == true{
            layer.cornerRadius = (self.frame.height/2)
                layer.masksToBounds = true
            }
            
        }
    }
    
    @IBInspectable var borderColor: UIColor{
        get{
            return UIColor.clear
        }
        set{
        self.layer.borderColor = newValue.cgColor
        }
    }
  
    @IBInspectable var borderWidth: CGFloat{
        get{
            return layer.borderWidth
        }
        set{
            layer.borderWidth = newValue
        }
    }
}

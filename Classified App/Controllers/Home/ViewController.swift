//
//  ViewController.swift
//  Classified App
//
//  Created by Syed  Rafay on 08/02/2021.
//  Copyright © 2021 rafay. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift
import RxCocoa
class ViewController: BaseViewController {

    @IBOutlet weak var myTableView: UITableView!
    let viewModel = ProductViewModel()
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viewModel.loading.bind(to: self.rx.isAnimating).disposed(by: self.disposeBag)
        self.viewModel.apiCallforProducts()
        self.viewModel.productsPublisher.subscribe(onNext: { [weak self](prod) in
            DispatchQueue.main.async {
                   
                          self?.myTableView.tableFooterView = UIView()
                          self?.myTableView.delegate = self
                          self?.myTableView.dataSource = self
                      }
            }).disposed(by: disposeBag)
    }
   

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destVC = segue.destination as? ProductDetailViewController{
            destVC.viewModel._singleProd = self.viewModel.products![(sender as? Int)!]
        }
    }
}

extension ViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let prod = self.viewModel.products{
        
            return prod.count
        }else{
            return 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ProductsTableViewCell
        cell?.isAccessibilityElement = true
        cell?.accessibilityLabel = "cell"
        let data = self.viewModel.products![indexPath.row]
        cell?.prodName.text = data.name ?? ""
        cell?.prodPrice.text = data.price ?? ""
        if let thumbnail = data.imageUrlsThumbnails{
            if thumbnail.count > 0{
        
                cell?.prodView.kf.setImage(with: URL(string:thumbnail.first ?? ""))
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toDetail", sender: indexPath.row)
    }
    
}

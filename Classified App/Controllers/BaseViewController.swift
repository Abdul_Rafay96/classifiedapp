//
//  BaseViewController.swift
//  Classified App
//
//  Created by Syed  Rafay on 09/02/2021.
//  Copyright © 2021 rafay. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
class BaseViewController: UIViewController {


    let  myActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
             myActivityIndicator.color = .red
             myActivityIndicator.center =  view.center
             
             myActivityIndicator.hidesWhenStopped = true
             
             view.addSubview(myActivityIndicator)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
     func startActivity(){
            self.myActivityIndicator.startAnimating()
    //        UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        
        func stopActivity(){
            self.myActivityIndicator.stopAnimating()
    //        UIApplication.shared.endIgnoringInteractionEvents()
        }
}

extension Reactive where Base:BaseViewController{
    internal var isAnimating:Binder<Bool>{
        return Binder(self.base,binding: { (vc,active) in
            if active{
                vc.startActivity()
            }else{
            vc.stopActivity()
            }
            
        })
    }
}

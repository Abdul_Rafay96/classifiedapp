//
//  ProductDetailViewController.swift
//  Classified App
//
//  Created by Syed  Rafay on 09/02/2021.
//  Copyright © 2021 rafay. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController {
    @IBOutlet weak var prodImg: UIImageView!
    @IBOutlet weak var prodName: UILabel!
    @IBOutlet weak var prodPrice: UILabel!
    
    var viewModel = ProductViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupUI()
    }
    
    func setupUI(){
        if let prod = self.viewModel._singleProd{
            self.prodImg.kf.setImage(with: URL(string: (prod.imageUrls?.first ?? "")))
            self.prodPrice.text = prod.price ?? ""
            self.prodName.text = prod.name ?? ""
        }
    }
}

//
//  RequestHandler.swift
//  Classified App
//
//  Created by Syed  Rafay on 09/02/2021.
//  Copyright © 2021 rafay. All rights reserved.
//

import Foundation

class RequestHandler{
    
    //private singleton
       private static var _obj:RequestHandler? = nil
       
       
       
       
       //singlton object
       class var sharedInstance: RequestHandler {
           get{
               if(_obj == nil){
                   _obj = RequestHandler()
               }
               
               let lockQueue = DispatchQueue(label: "obj")
               return lockQueue.sync{
                   return _obj!
               }
           }
       }
    
    func getProducts(success:@escaping(_ products:[[String:Any]])->Void){
            let session = URLSession.shared
            let url = URL(string: "https://ey3f2y0nre.execute-api.us-east-1.amazonaws.com/default/dynamodb-writer")!
        DispatchQueue.global(qos: .background).async {
            
        
          let task = session.dataTask(with: url, completionHandler: { data, response, error in
                // Check the response
                print(response)
                
                // Check if an error occured
                if error != nil {
                    // HERE you can manage the error
                    print(error)
                    return
                }
                
                // Serialize the data into an object
                do {
    //                let json = try JSONDecoder().decode(Products.self, from: data! )
                        //try JSONSerialization.jsonObject(with: data!, options: [])
    //                let jsonEncoder = JSONEncoder()
    //                let encodedJson = try jsonEncoder.encode(data!)
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    
//                    print(json)
                    if let dict = json{
                        if let productArr = dict["results"] as? [[String:Any]]{
                            success(productArr)
                        }
                    }
                    
                } catch {
                    print("Error during JSON serialization: \(error.localizedDescription)")
                }
                
            })
            task.resume()
        }
    }
}

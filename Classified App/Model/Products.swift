//
//  Products.swift
//
//  Created by Syed  Rafay on 08/02/2021
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Products: NSCoding,Decodable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let imageIds = "image_ids"
    static let name = "name"
    static let imageUrls = "image_urls"
    static let createdAt = "created_at"
    static let imageUrlsThumbnails = "image_urls_thumbnails"
    static let price = "price"
    static let uid = "uid"
  }

  // MARK: Properties
  public var imageIds: [String]?
  public var name: String?
  public var imageUrls: [String]?
  public var createdAt: String?
  public var imageUrlsThumbnails: [String]?
  public var price: String?
  public var uid: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.imageIds].array { imageIds = items.map { $0.stringValue } }
    name = json[SerializationKeys.name].string
    if let items = json[SerializationKeys.imageUrls].array { imageUrls = items.map { $0.stringValue } }
    createdAt = json[SerializationKeys.createdAt].string
    if let items = json[SerializationKeys.imageUrlsThumbnails].array { imageUrlsThumbnails = items.map { $0.stringValue } }
    price = json[SerializationKeys.price].string
    uid = json[SerializationKeys.uid].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = imageIds { dictionary[SerializationKeys.imageIds] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = imageUrls { dictionary[SerializationKeys.imageUrls] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = imageUrlsThumbnails { dictionary[SerializationKeys.imageUrlsThumbnails] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = uid { dictionary[SerializationKeys.uid] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.imageIds = aDecoder.decodeObject(forKey: SerializationKeys.imageIds) as? [String]
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.imageUrls = aDecoder.decodeObject(forKey: SerializationKeys.imageUrls) as? [String]
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.imageUrlsThumbnails = aDecoder.decodeObject(forKey: SerializationKeys.imageUrlsThumbnails) as? [String]
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? String
    self.uid = aDecoder.decodeObject(forKey: SerializationKeys.uid) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(imageIds, forKey: SerializationKeys.imageIds)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(imageUrls, forKey: SerializationKeys.imageUrls)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(imageUrlsThumbnails, forKey: SerializationKeys.imageUrlsThumbnails)
    aCoder.encode(price, forKey: SerializationKeys.price)
    aCoder.encode(uid, forKey: SerializationKeys.uid)
  }

}

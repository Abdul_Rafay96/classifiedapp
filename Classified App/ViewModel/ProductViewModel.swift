//
//  MainViewModel.swift
//  Classified App
//
//  Created by Syed  Rafay on 08/02/2021.
//  Copyright © 2021 rafay. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
class ProductViewModel{
    private var _products:[Products]?
    
    public let loading:PublishRelay<Bool> = PublishRelay()
    var productsPublisher:PublishRelay<[Products]> = PublishRelay()
    
    var products:[Products]?  {
        get{
            return self._products
        }
        set{
            self._products = newValue
        }
    }
    
    var _singleProd : Products?
    
    func apiCallforProducts(){
        self.loading.accept(true)
        RequestHandler.sharedInstance.getProducts { [weak self] (responses) in
            let op1 = BlockOperation{
                if responses.count > 0{
                    self?._products = [Products]()
                    for response in responses{
                        let pro = Products.init(object: response)
                        self?._products!.append(pro)
                    }
                }
            }
            let op2 = BlockOperation{
                self?.loading.accept(false)
                self?.productsPublisher.accept((self?._products)!)
            }
            op2.addDependency(op1)
            op1.start()
            op2.start()
        }
    }
}

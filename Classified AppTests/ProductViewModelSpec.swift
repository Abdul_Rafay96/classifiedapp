//
//  ProductViewModelSpec.swift
//  Classified App
//
//  Created by Syed  Rafay on 09/02/2021.
//  Copyright © 2021 rafay. All rights reserved.
//

import XCTest
@testable import Classified_App

class ProductViewModelSpec: XCTestCase {

    var viewModel : ProductViewModel!
    override func setUp() {
        viewModel = ProductViewModel()
        viewModel.apiCallforProducts()
        
    }

    func testIfgettingData(){
        
        XCTAssertNil(self.viewModel.products)
    }
}
